<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rejas;

class InicioHerreriaControlador extends Controller
{
    public function Inicio(){
        return view('inicio');
    }

    public function nuevaReja(){
        return view('nuevaReja');
    }

    public function crearNuevaReja(Request $request){
        /*$reja = new Rejas();
        $reja->nombre = $request->nombre;
        $reja->descripcion = $request->descripcion;
        $reja->precio = $request->precio;
        $reja->imagen = $request->imagen;
        $reja->disponible = $request->disponible;
        $reja->save();*/  
        if($request->hasFile('imagen')){
           $file = $request->file('imagen');
           $name = time().$file->getClientOriginalName();
           $file->move(public_path()."/img/",$name);
           return $name;  
        }
        
    }
}
