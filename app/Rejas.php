<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rejas extends Model
{
    protected $fillable = [
        'nombre', 'descripcion', 'precio','disponible','imagen'
    ];
}
