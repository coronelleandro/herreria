@extends('plantilla')

@section('seccion')
<div class="mt-5 mb-5">
    <div class="border p-5">
        <form action="crear-reja" method="post" enctype="multipart/form-data">
            @csrf
            <input type="text" name="nombre" class="form-control mb-3 mt-3" placeholder="nombre">
                
            <textarea name="descripcion" class="form-control mb-3" placeholder="descripcion"></textarea>
        
            <input type="text" name="precio" class="form-control mb-3" placeholder="precio">

            <input type="file" name="imagen" class="mb-3" placeholder="imagen"><br>
            
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary mb-3  col-md-5">Guardar</button>
            </div>
        </form>
    </div>
</div>    
@endsection