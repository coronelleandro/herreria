<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/plantilla.css">
    <title>Hello, world!</title>
  </head>
  <body>
    <main class="bg-dark text-white d-flex justify-content-between">
        <nav class="container d-flex justify-content-between align-items-center pt-1 pb-1">
            <div class="">
                <h3>Logo</h3>
            </div> 
            <div class="d-flex"> 
                <a class="pt-1 pb-1 pl-3 pr-3 bg-white text-dark rounded ml-1 mr-1">Inicio</a>
                <a class="pt-1 pb-1 pl-3 pr-3 bg-white text-dark rounded ml-1 mr-1">link 2</a>
                <a class="pt-1 pb-1 pl-3 pr-3 bg-white text-dark rounded ml-1 mr-1">link 3</a>
            </div>
        </nav>    
    </main>
    <div class="container">
    @yield('seccion')
  
    </div>
    <footer class="bg-dark text-white">
        <div class="container">
            <h4>footer</h4>
        <div>
    </footer>
      <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>